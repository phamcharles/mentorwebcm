<?php require 'PHP/header.php'; ?>

<body class="page page-login">
    <a href="#content" class="sr-only">Skip to content</a>
    
    <?php require 'PHP/navbar.php'; ?>

    <div id="content">
        <div class="container">
            <!-- Login form -->
            <form class="form-login form-wrapper form-narrow" role="form" id="login_form">
                <h3 class="title-divider">
                    <span>Login</span>
                    <small>Not signed up? <a href="pricing.php">Sign up here</a>.</small>
                </h3>
                <div class="form-group">
                    <label class="sr-only" for="login_un">Username</label>
                    <input type="text" class="form-control" id="login_un" name="login_un" placeholder="Username">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="login_pw">Password</label>
                    <input type="password" class="form-control" id="login_pw" name="login_pw" placeholder="Password">
                </div>
                <button type="button" class="btn btn-primary" id="login_submit">Login</button>
                | <a href="#">Forgotten Password?</a>
            </form>
        </div>
    </div>

    <!-- FOOTER -->
    <?php require 'PHP/footer.php'; ?>
    <?php require 'PHP/footer_scripts.php'; ?>

    <!-- Page Specific Scripts -->
    <script src="js/login.js"></script>
</body>

</html>