<?php
	ob_start();	//"Start Remembering everything that would normally be outputted"
	session_start();

	$m=new MongoClient();

	$db=$m->test;

	$collection=$db->test_insert;

	$searchResult=array("username"=>$_POST["login_un"]);
	$searchPass=array("password"=>$_POST["login_pw"]);

	//Query database
	$UserResult=$collection->find($searchResult);
	$PassResult=$collection->find($searchPass);

	$UserResult=$UserResult->getNext();
	$PassResult=$PassResult->getNext();


	//Mongo Status Return Variables
	$success=array('status'=>'success');
	$failure=array('status'=>'failure');

	//Check the results and transmit appropriate data back
	if($UserResult && $PassResult)
	{
		echo json_encode($success);
		session_regenerate_id();
		$_SESSION['sess_user_id']=$UserResult['_id'];
		$_SESSION['sess_username']=$UserResult['username'];
		$_SESSION['sess_firstname']=$UserResult['name']['first'];
		$_SESSION['sess_lastname']=$UserResult['name']['last'];
		$_SESSION['sess_email']=$UserResult['contact']['email'];
		$_SESSION['sess_phone']=$UserResult['contact']['phone'];
		$_SESSION['sess_city']=$UserResult['contact']['address']['city'];
		$_SESSION['sess_job']=$UserResult['job'];
		$_SESSION['sess_education']=$UserResult['education'];
		$_SESSION['sess_interests']=$UserResult['interests'];
		$_SESSION['sess_status']='online';
		session_write_close();
	}
	else
	{
		echo json_encode($failure);
	}
	//One issue arises. Although we will have to ensure that emails
	//and usernames are unique in signup.php, passwords will not
	//be unique to users. have to ensure that ID for both username
	//password match. (document ID)

?>
