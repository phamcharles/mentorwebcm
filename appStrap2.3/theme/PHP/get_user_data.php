<?php
	$m=new MongoClient();

	$db=$m->test;

	$collection=$db->test_insert;

	$query = array("_id" => $_POST["url_id"]);
	$projection = array( '_id' => true, 'username' => true, 'name' => true, 'contact' => true, 'education' => true, 'job' => true, 'interests' => true);

	$cursor = $collection->find($query, $projection);
	$list = mongoToArray($cursor);

	echo json_encode($list);
?>