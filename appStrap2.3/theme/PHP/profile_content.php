<div id="content">
    <div class="container">
        <h2 class="title-divider">
            <span class="de-em">Profile</span>
            <small><a href="MW-HOME.php"><i class="fa fa-arrow-left"></i> Back to home page</a>
            </small>
        </h2>
        <div class="row">
            <!--Main Content-->
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6" style="margin-bottom:20px">
                        <h3 class="title media-heading text-left" style="margin-bottom:10px"><?php echo $_SESSION[ 'sess_firstname']; ?>'s Profile</h3>
                        <form class="form-login form-wrapper form-medium" role="form" id="profile_form">
                            <p>
                                <strong>Username:</strong> <span id='profile_username'><?php echo $_SESSION[ 'sess_username']; ?></span>
                            </p>
                            <p>
                                <strong>Email:</strong> <span id='profile_email'><?php echo $_SESSION[ 'sess_email']; ?></span>
                            </p>
                            <p>
                                <strong>First Name:</strong> <span id='profile_firstname'><?php echo $_SESSION[ 'sess_firstname']; ?></span>
                            </p>
                            <p>
                                <strong>Last Name:</strong> <span id='profile_lastname'><?php echo $_SESSION[ 'sess_lastname']; ?></span>
                            </p>
                            <p>
                                <strong>Phone:</strong> <span id='profile_phone'><?php echo $_SESSION[ 'sess_phone']; ?></span>
                            </p>
                            <p>
                                <strong>City:</strong> <span id='profile_city'><?php echo $_SESSION[ 'sess_city']; ?></span>
                            </p>
                            <p>
                                <strong>Job:</strong> <span id='profile_job'><?php echo $_SESSION[ 'sess_job']; ?></span>
                            </p>
                            <p>
                                <strong>Education:</strong> <span id='profile_education'><?php echo $_SESSION[ 'sess_education']; ?></span>
                            </p>
                            <p>
                                <strong>Interests:</strong> <span id='profile_interests'><?php echo $_SESSION[ 'sess_interests']; ?></span>
                            </p>
                        </form>
                    </div>
                    <div class="col-md-6" style="margin-bottom:20px">
                        <h3 class="title media-heading text-left" style="margin-bottom:10px">Leave <?php $_SESSION[ 'sess_firstname']?>a message</h3>
                        <form class="form-login form-wrapper form-medium" role="form" id="comment_form">
                            <div class="form-group">
                                <label class="sr-only" for="comment_name">Name</label>
                                <input readonly type="text" class="form-control" id="comment_firstname" placeholder="Name">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="comment-name">Email</label>
                                <input readonly type="email" class="form-control" id="comment_email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="comment_comment">Comment</label>
                                <textarea rows="8" class="form-control" id="comment_comment" placeholder="Comment"></textarea>
                            </div>
                            <button type="submit" class="btn btn-primary" id='comment_submit'>Submit</button>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-3 text-center">
                    <h3 class="title media-heading" style="margin-bottom:10px">Actions</h3>
                    <div class="span6">
                        <input type="button" class="btn btn-primary" id='add_mentee' value="Mentor Them"></input>
                        <input type="button" class="btn btn-primary" id='remove_mentee' style="margin-top:20px" value="Stop Teaching Them"></input>
                    </div>
                    <div class="span6">
                        <input type="button" class="btn btn-primary" id='add_mentor' style="margin-top:20px" value="Be Their Mentee"></input>
                        <input type="button" class="btn btn-primary" id='remove_mentor' style="margin-left:10px; margin-top:20px" value="Stop Learning From Them"></input>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>