<?php
	session_start();	

	$m=new MongoClient();

	$db=$m->test;

	$collection=$db->test_insert;

	$query = array("username" => $_SESSION['sess_username']);
	$update = array('$set' => array(
			"name.first"=>$_POST["text_firstname"],
			"name.last"=>$_POST["text_lastname"],
			"contact.phone"=>$_POST["text_phone"],
			"contact.address.city"=>$_POST["text_city"],
			"job"=>$_POST["text_job"],
			"education"=>$_POST["text_education"],
			"interests"=>$_POST["text_interests"]
		)
	);


	$collection->update($query, $update);

	$_SESSION['sess_firstname']=$_POST["text_firstname"];
	$_SESSION['sess_lastname']=$_POST["text_lastname"];
	$_SESSION['sess_phone']=$_POST["text_phone"];
	$_SESSION['sess_city']=$_POST["text_city"];
	$_SESSION['sess_job']=$_POST["text_job"];
	$_SESSION['sess_education']=$_POST["text_education"];
	$_SESSION['sess_interests']=$_POST["text_interests"];	
?>
