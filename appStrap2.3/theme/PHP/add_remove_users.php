<?php
	/* this has not been tested */

	require "debug.php";
	require "functions.php";

	function addMentee($id) {
		session_start();
		$m=new MongoClient();
		$db=$m->test;
		$collection=$db->test_insert;

		$query = array("username" => $_SESSION['sess_username']);
		$update = array('$push' => array('mentees' => $id));

		$collection->update($query, $update);
		session_write_close();
		return "SUCCESS";
	}
	function removeMentee($id) {
		session_start();
		$m=new MongoClient();
		$db=$m->test;
		$collection=$db->test_insert;

		$query = array("username" => $_SESSION['sess_username']);
		$update = array('$pull' => array('mentees' => $id));

		$collection->update($query, $update);
		session_write_close();
		return "SUCCESS";
	}
	function addMentor($id) {
		session_start();
		$m=new MongoClient();
		$db=$m->test;
		$collection=$db->test_insert;

		$query = array("username" => $_SESSION['sess_username']);
		$update = array('$push' => array('mentors' => $id));

		$collection->update($query, $update);
		session_write_close();
		return "SUCCESS";
	}
	function removeMentor($id) {
		session_start();
		$m=new MongoClient();
		$db=$m->test;
		$collection=$db->test_insert;

		$query = array("username" => $_SESSION['sess_username']);
		$update = array('$pull' => array('mentors' => $id));

		$collection->update($query, $update);
		session_write_close();
		return "SUCCESS";
	}

	$action = $_POST["action"];
	$data = $_POST["data"];

	$status = "";
	switch ($action) {
		case 'addMentee':
			$status = addMentee($data);
			break;
		case 'removeMentee':
			$status = removeMentee($data);
			break;
		case 'addMentor':
			$status = addMentor($data);
			break;
		case 'removeMentor':
			$status = removeMentor($data);
			break;
		default:
			$status = "FAILURE";
			break;
	}
	echo $status;
?>
