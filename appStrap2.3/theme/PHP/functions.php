<?php

function session_write($key, $value) {
	session_start();
	$_SESSION["$key"] = $value;
	session_write_close();
	session_destroy();
}
function session_read($key) {
	session_start();
	$value = $_SESSION["$key"];
	session_write_close();
	session_destroy();
	return $value;
}
function mongoToArray($cursor) {
	$list = array();
	foreach ($cursor as $doc) {
		array_push($list, $doc);
	}
	return $list;
} 
function getSafeReturn($key){
	session_start();	

	$m=new MongoClient();

	$db=$m->test;

	$collection=$db->test_insert;

	$query = array("_id" => $key);
	$projection = array( '_id' => true, 'username' => true, 'name' => true, 'contact' => true, 'education' => true, 'job' => true, 'interests' => true);

	$cursor = $collection->find($query, $projection);
	$list = mongoToArray($cursor);

	echo json_encode($list);
}
?>