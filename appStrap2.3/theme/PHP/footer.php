<div id="content-below" class="wrapper">
    <div class="container">
        <div class="row">
        </div>
    </div>
</div>

<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-3 col">
                <div class="block contact-block">
                    <!--@todo: replace with company contact details-->
                    <h3>Contact Us</h3>
                    <address>
                        <ul class="fa-ul">
                            <li>
                                <abbr title="Phone"><i class="fa fa-li fa-phone"></i>
                                </abbr>1-800-MENTORME</li>
                            <li>
                                <abbr title="Email"><i class="fa fa-li fa-envelope"></i>
                                </abbr>info@mentorweb.com</li>
                            <li>
                                <abbr title="Address"><i class="fa fa-li fa-home"></i>
                                </abbr>San Jose, CA 95192</li>
                        </ul>
                    </address>
                </div>
            </div>
            <div class="col-md-5 col">
                <div class="block">
                    <h3>About Us</h3>
                    <p>Making it easier for individuals of all levels help each other.</p>
                    <a href="MW-ABOUT.php" class="btn btn-sm btn-primary">Find out more</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div id="toplink"><a href="#top" class="top-link" title="Back to top">Back To Top <i class="fa fa-chevron-up"></i></a>
            </div>
            <div class="subfooter">
                <div class="col-md-6">
                </div>
                <div class="col-md-6 pull-right">
                	<ul class="list-inline footer-menu">
                    		<p>MentorWeb 2014</p>
			</ul>
                </div>
            </div>
        </div>
    </div>
</footer>
