<?php if ($_SESSION['sess_status'] == "online") { ?>
		<div class="btn-group user-menu pull-right dropdown"> 
					<a class="signup dropdown-toggle btn btn-primary" data-toggle="dropdown"><?php echo $_SESSION['sess_firstname']; ?></a>
						<ul class="dropdown-menu" role="menu" aria-lebelledby="user_drop" id='user_form'>
							<li role="presentation"><a role="menuitem" href="MW-EDITPROFILE.php" tabindex="-1" class="menu-item" id='user_profile'>Edit Profile</a>
							</li>
							<li role="presentation"><a role="menuitem" tabindex="-1" class="menu-item" id='user_signout'>Sign Out</a>
							</li>
						</ul>
					</div>
<?php }
else { ?>
					<div class="btn-group user-menu pull-right"> <a href="MW-SIGNUP.php" class="btn btn-primary signup">Sign Up</a>  <a href="MW-LOGIN.php" class="btn btn-primary dropdown-toggle login">Login</a> 
					</div>
<?php } ?>
