<?php
	require "debug.php";
	require "functions.php";
	function getMentees() {
		session_start();
		$m=new MongoClient();
		$db=$m->test;
		$collection=$db->test_insert;

		$query = array("username"=> array('$ne' => $_SESSION['sess_username']));
		$projection = array( 'contact' => true, '_id' => true, 'education' => true, 'name' => true );

		$cursor = $collection->find($query, $projection);
		$list = mongoToArray($cursor);
		session_write_close();
		return $list;
	}
	function getMentors() {
		session_start();
		$m=new MongoClient();
		$db=$m->test;
		$collection=$db->test_insert;

		$query = array("username"=> array('$ne' => $_SESSION['sess_username']));
		$projection = array( 'contact' => true, '_id' => true, 'education' => true, 'name' => true );

		$cursor = $collection->find($query, $projection);
		$list = mongoToArray($cursor);

		session_write_close();
		return $list;
	}

	function getMyMentees() {
		session_start();
		$m=new MongoClient();
		$db=$m->test;
		$collection=$db->test_insert;

		/** get list of mentees **/
		$query = array("username" => $_SESSION['sess_username']);
		$projection = array('mentees' => true );
		$cursor = $collection->find($query, $projection);
		$mentees = mongoToArray($cursor);

		$mentees = $mentees[0]["mentees"];

		for ($i=0; $i < sizeof($mentees); $i++) {
			$mentees[$i] = new MongoId($mentees[$i]);
		}

		$query = array("_id" => array( '$in' => $mentees));
		$projection = array( 'contact' => true, '_id' => true, 'education' => true, 'name' => true );
		$cursor = $collection->find($query, $projection);
		$list = mongoToArray($cursor);

		session_write_close();
		return $list;
	}

	function getMyMentors() {
				session_start();
		$m=new MongoClient();
		$db=$m->test;
		$collection=$db->test_insert;

		/** get list of mentors **/
		$query = array("username" => $_SESSION['sess_username']);
		$projection = array('mentors' => true );
		$cursor = $collection->find($query, $projection);
		$mentors = mongoToArray($cursor);

		$mentors = $mentors[0]["mentors"];

		for ($i=0; $i < sizeof($mentors); $i++) {
			$mentors[$i] = new MongoId($mentors[$i]);
		}

		$query = array("_id" => array( '$in' => $mentors));
		$projection = array( 'contact' => true, '_id' => true, 'education' => true, 'name' => true );
		$cursor = $collection->find($query, $projection);
		$list = mongoToArray($cursor);

		session_write_close();
		return $list;
	}

	$action = $_POST["action"];
	//$data = $_POST["data"];

	$status = "";
	switch ($action) {
		case 'mentees':
			$status = json_encode(getMentees());
			break;
		case 'mentors':
			$status = json_encode(getMentors());
			break;
		case 'myMentees':
			$status = json_encode(getMyMentees());
			break;
		case 'myMentors':
			$status = json_encode(getMyMentors());
			break;
		default:
			$status = "FAILURE";
			break;
	}
	echo $status;
?>
