<?php
	session_start();
	$m=new MongoClient();

	$db=$m->test;

	$collection=$db->test_insert;

	//The following are the query values
	$queryUsername=array("username"=>$_POST["signup_un"]);
	$queryEmail=array("contact.email"=>$_POST["signup_eml"]);

	//The following is for querying DB and document extraction
	
	$cursor = $collection->find($queryUsername);
	foreach($cursor as $doc)
	{
		$usernameResults=$doc;	
	}

	$cursor = $collection->find($queryEmail);
	foreach($cursor as $doc)
	{
		$emailResults=$doc;
	}


	if(!$usernameResults && !$emailResults){
	//The following is the logic for success or failure
	$document=array("username"=>$_POST["signup_un"],
					"password"=>$_POST["signup_pw"],
					"role"=>"User",
					"name"=>array("first"=>$_POST["signup_fn"],
								  "mid"=>null,
			      				  "last"=>$_POST["signup_ln"]),
					"contact"=>array("phone"=>null,
							 "email"=>$_POST["signup_eml"],
							 "address"=>array("address1"=>null,
									  "address2"=>null,
									  "address3"=>null,
									  "city"=>null,
									  "zip"=>null)),
					"interests"=>array(),
					"goals"=>array(),
					"mentors"=>array(),
					"mentees"=>array(),
					"education"=>null,
					"job"=>null,
					);

		$collection->insert($document);
		$status=array("status"=>"success");
		session_regenerate_id();
		$_SESSION['sess_user_id']=$document['_id'];
		$_SESSION['sess_username']=$document['username'];
		$_SESSION['sess_firstname']=$document['name']['first'];
		$_SESSION['sess_lastname']=$document['name']['last'];
		$_SESSION['sess_email']=$document['contact']['email'];
		$_SESSION['sess_phone']=$document['contact']['phone'];
		$_SESSION['sess_city']=$document['contact']['address']['city'];
		$_SESSION['sess_job']=$document['job'];
		$_SESSION['sess_education']=$document['education'];
		$_SESSION['sess_interests']=$document['interests'];
		$_SESSION['sess_status']='online';
		session_write_close();		
		echo json_encode($status);
	}
	else if($usernameResults)
	{
		$status=array("status"=>"Username is taken!");
		echo json_encode($status);
	}
	else if($emailResults)
	{
		$status=array("status"=>"Email already in use!");
		echo json_encode($status);
	}
	
?>
