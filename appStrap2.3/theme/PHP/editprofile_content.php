<div id="content">
    <div class="container">
        <h2 class="title-divider">
            <span class="de-em">Profile</span>
            <small><a href="MW-HOME.php"><i class="fa fa-arrow-left"></i> Back to home page</a>
            </small>
        </h2>
        <div class="row">
            <!--Main Content-->
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-6">
                        <h3 class="title media-heading">
                            <?php echo $_SESSION[ 'sess_firstname']; ?>'s Profile</h3>
                        <form class="form-login form-wrapper form-medium" role="form" id="profile_form">
                            <p>
                                <strong>Username:</strong> <span id='profile_username'><?php echo $_SESSION[ 'sess_username']; ?></span>
                            </p>
                            <p>
                                <strong>Email:</strong> <span id='profile_email'><?php echo $_SESSION[ 'sess_email']; ?></span>
                            </p>
                            <p>
                                <strong>First Name:</strong> <span id='profile_firstname'><?php echo $_SESSION[ 'sess_firstname']; ?></span>
                            </p>
                            <p>
                                <strong>Last Name:</strong> <span id='profile_lastname'><?php echo $_SESSION[ 'sess_lastname']; ?></span>
                            </p>
                            <p>
                                <strong>Phone:</strong> <span id='profile_phone'><?php echo $_SESSION[ 'sess_phone']; ?></span>
                            </p>
                            <p>
                                <strong>City:</strong> <span id='profile_city'><?php echo $_SESSION[ 'sess_city']; ?></span>
                            </p>
                            <p>
                                <strong>Job:</strong> <span id='profile_job'><?php echo $_SESSION[ 'sess_job']; ?></span>
                            </p>
                            <p>
                                <strong>Education:</strong> <span id='profile_education'><?php echo $_SESSION[ 'sess_education']; ?></span>
                            </p>
                            <p>
                                <strong>Interests:</strong> <span id='profile_interests'><?php echo $_SESSION[ 'sess_interests']; ?></span>
                            </p>
                            <button type="submit" class="btn btn-primary" id='profile_edit'>Edit Profile</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>