    <div id="navigation" class="wrapper">
        <div class="navbar-static-top">

            <!--Header & Branding region-->
            <div class="header">
                <div class="header-inner container">
                    <div class="row">
                        <div class="col-md-8">
                            <!--branding/logo-->
                            <a class="navbar-brand" href="MW-HOME.php" title="Home">
                                <h1>
                                    <span>Mentor</span>Web
                                </h1>
                            </a>
                            <div class="slogan">Help when you need it</div>
                        </div>

                        <!--header rightside-->
                        <div class="col-md-4">
                            <!--social media icons-->
                            <div class="social-media">
                                <!--@todo: replace with company social media details-->
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container" data-toggle="clingify">
                <div class="navbar">
                    <!--
      mobile collapse menu button
      - data-toggle="toggle" = default BS menu
      - data-toggle="jpanel-menu" = jPanel Menu
      -->
                    <a class="navbar-btn" data-toggle="jpanel-menu" data-target=".navbar-collapse">
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                        <span class="bar"></span>
                    </a>

                    <!--user menu-->
                    <?php require 'PHP/session_nav.php'; ?>

                    <!--everything within this div is collapsed on mobile-->
                    <div class="navbar-collapse collapse">

                        <!--main navigation-->
                        <ul class="nav navbar-nav" id='nav_form'>
                            <li class="home-link"><a href="MW-HOME.php"><i class="fa fa-home"></i><span class="hidden">Home</span></a>
                            </li>
                            <li class="dropdown"> <a href="MW-ABOUT.php" class="dropdown-toggle" id="about-drop" data-toggle="dropdown">About +</a> 
                                <!-- Dropdown Menu -->
                                <ul class="dropdown-menu" role="menu" aria-labelledby="about-drop">
                                    <li role="presentation"><a role="menuitem" href="MW-ABOUT.php" tabindex="-1" class="menu-item">About Us</a>
                                    </li>
                                    <li role="presentation"><a role="menuitem" href="MW-TEAM.php" tabindex="-1" class="menu-item">Our Team</a>
                                    </li>
                                    <li role="presentation"><a role="menuitem" href="MW-CONTACT.php" tabindex="-1" class="menu-item">Contact</a>
                                    </li>
                                </ul>
                            </li>
<!-- add quick access
                            <li class="dropdown active"> <a href="features.php" class="dropdown-toggle menu-item" id="features-drop" data-toggle="dropdown">Quick Menu +</a> 
                                <!-- Dropdown Menu - Mega Menu
                                <ul class="dropdown-menu mega-menu" role="menu" aria-labelledby="features-drop">
                                    <li role="presentation" class="dropdown-header"></li>
                                    <li role="presentation">
                                        <ul class="row list-unstyled" role="menu">
                                            <li class="col-md-4" role="presentation">
                                                <a role="menuitem" href="MW-MESSAGECENTERMENTEE.php" class="img-link">
                                                    <img src="img/features/message3.png" alt="Feature 1" />
                                                </a>
                                                <a role="menuitem" href="MW-MESSAGECENTERMENTEE.php" tabindex="-1" class="menu-item">
                                                    <strong>Messages</strong>
                                                </a>
                                                <span>View messages from your mentors!</span>
                                            </li>
                                            <li class="col-md-4" role="presentation">
                                                <a role="menuitem" href="MW-MYMENTORS.php" class="img-link">
                                                    <img src="img/features/professor.png" alt="Feature 2" />
                                                </a>
                                                <a role="menuitem" href="MW-MYMENTORS.php" tabindex="-1" class="menu-item">
                                                    <strong>My Mentors</strong>
                                                </a>
                                                <span>View all of your mentors!</span>
                                            </li>
                                            <li class="col-md-4" role="presentation">
                                                <a role="menuitem" href="MW-PENDINGMENTORS.php" class="img-link">
                                                    <img src="img/features/hands.jpg" alt="Feature 2" />
                                                </a>
                                                <a role="menuitem" href="MW-PENDINGMENTORS.php" tabindex="-1" class="menu-item">
                                                    <strong>Pending Mentors</strong>
                                                </a>
                                                <span>View who wants to be your mentor!</span>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li> 
-->
                        </ul>
                        </li>
                    </div>
                    <!--/.navbar-collapse -->
                </div>
            </div>
        </div>
    </div>