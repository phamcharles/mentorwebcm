<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>MentorWeb</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- @todo: fill with your company info or remove -->
    <meta name="description" content="">
    <meta name="author" content="Themelize.me">

    <!-- Bootstrap CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="css/font-awesome.min.css" rel="stylesheet">

    <!-- Plugins -->
    <link href="plugins/prism/prism.css" media="screen" rel="stylesheet" />
    <link href="plugins/slider-revolution/rs-plugin/css/settings.css?v=4.2" media="screen" rel="stylesheet" />
    <link href="plugins/animate/animate.css" rel="stylesheet">
    <link href="plugins/flexslider/flexslider.css" rel="stylesheet">
    <link href="plugins/clingify/clingify.css" rel="stylesheet">

    <!-- Theme style -->
    <link href="css/theme-style.min.css" rel="stylesheet">

    <!--Your custom colour override-->
    <link href="#" id="colour-scheme" rel="stylesheet">

    <!-- Your custom override -->
    <link href="css/custom-style.css" rel="stylesheet">

    <!-- Le fav and touch icons - @todo: fill with your icons or remove -->
    <link rel="shortcut icon" href="img/icons/favicon.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/icons/114x114.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/icons/72x72.png">
    <link rel="apple-touch-icon-precomposed" href="img/icons/default.png">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,300|Rambla|Calligraffitti' rel='stylesheet' type='text/css'>

    <!--Retina.js plugin - @see: http://retinajs.com/-->
    <script src="plugins/retina/js/retina-1.1.0.min.js"></script>
</head>