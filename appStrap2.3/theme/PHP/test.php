<?php
session_start();
$errPass=NULL;
$errFirst=NULL;
$errLast=NULL;
$errUser=NULL;
$errEmail=NULL;

$error=NULL;
$success=NULL;

// Password must be strong
if(preg_match("/^.*(?=.{8,})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).*$/", $_POST["pass"]) === 0){
	$errPass = '<p class="errText">Password must be at least 8 characters		and must contain at least one lower case letter, one upper case letter and one digit</p>';
	$_SESSION['errorPassword']=$errPass;
}
else if($_POST['first']=='first name')
{
	$errFirst='<p class="errText">Please enter a first name</p>';
	$_SESSION['errorFirst']=$errFirst;
}
else if($_POST['last']=='last name')
{
	$errLast='<p class="errText">Please enter a last name</p>';
	$_SESSION['errorLast']=$errLast;
}
else if($_POST['username']=='username')
{
	$errUser='<p class="errTest">Please enter a username</p>';
	$_SESSION['errorUser']=$errUser;
}
else
{
	$m=new MongoClient();

	$db=$m->test;

	$collection=$db->test_insert;

	$document=array("username"=>$_POST["username"],
			"name"=>array(	"prefix"=>null,
			      		"first"=>$_POST["first"],
			      		"mid"=>null,
			      		"last"=>$_POST["last"],
			      		"suffix"=>null),
			"email"=>$_POST["emaill"],
			"password"=>$_POST["pass"]);

	$collection->insert($document);
	
	$success='<p>You have successfully create an account!</p>';
	$_SESSION['success']=$success;
}

if(isset($success))
{
	header("Location: ../MW-FRONT.htm");
}
else
{
	header("Location: http://www.mycareergem.com/mentorwebcm/appStrap2.3/theme/MW-FRONT.htm");
}
?>




