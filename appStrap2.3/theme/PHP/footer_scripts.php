<!--Scripts -->
<script src="js/jquery.min.js"></script>
<script src="js/jquery-migrate-1.2.1.min.js"></script>
<!--Legacy jQuery support for quicksand plugin-->

<!-- Bootstrap JS -->
<script src="js/functions.js"></script>
<script src="js/ajax-communicator.js"></script>
<script src="js/bootbox.min.js"></script>
<script src="js/bootstrap.min.js"></script>

<!--JS plugins-->
<script src="plugins/prism/prism.js"></script>
<script src="plugins/slider-revolution/rs-plugin/js/jquery.themepunch.plugins.min.js?v=4.2"></script>
<script src="plugins/slider-revolution/rs-plugin/js/jquery.themepunch.revolution.min.js?v=4.2"></script>
<script src="plugins/flexslider/jquery.flexslider-min.js"></script>
<script src="plugins/clingify/jquery.clingify.min.js"></script>
<script src="plugins/jPanelMenu/jquery.jpanelmenu.min.js"></script>
<script src="plugins/jRespond/js/jRespond.js"></script>
<script src="plugins/quicksand/jquery.quicksand.js"></script>

<!--Custom scripts mainly used to trigger libraries -->
<script src="js/script.min.js"></script>
<script src="js/session_handler.js"></script>

