<?php
	session_start();
	//MongoDB initialization
	$m=new MongoClient();

	$db=$m->test;

	$collection=$db->test_insert;
/////////////////////////////////////////////////////////////////////////////////////////
	
	//Parse search value
	$keywords = preg_split("/[\s,]+/",$_POST["search_value"]);
	$count=sizeof($keywords); //Number of strings
	$index=0; //starting position of array

////////////////////////////////////////////////////////////////////////////////////////

	//Search value passed to search results page
	$_SESSION["search_value"]=$_POST["search_value"];

///////////////////////////////////////////////////////////////////////////////////////
		
	//Query the individual keywords
	while($count > 0){
		//The following are the query values
		$queryUsername=array("username"=>$keywords[$index]);
		$queryFirstName=array("name.first"=>$keywords[$index]);
		$queryLastName=array("name.last"=>$keywords[$index]);
		$queryInterest=array("interests"=>$keywords[$index]);
	
		//The following is for querying DB and document extraction
		$cursor = $collection->find($queryUsername);
		foreach($cursor as $doc)
		{
			$usernameResults=$doc;
		}

		$cursor = $collection->find($queryFirstName);
		foreach($cursor as $doc)
		{
			$firstNameResults=$doc;
		}
	
		$cursor = $collection->find($queryLastName);
		foreach($cursor as $doc)
		{
			$lastNameResults=$doc;
		}

		$cursor = $collection->find($queryInterest);
		foreach($cursor as $doc)
		{
			$interestResults=$doc;
		}
			$count--;
			$index++;	
	}
	
////////////////////////////////////////////////////////////////////////////////////////
	$success=array("status"=>"success");
	$failure=array("status"=>"failure");

	//The following is the success and failure logic
	if($usernameResults)
	{
		echo json_encode($success);
	}
	else if($firstNameResults)
	{
		echo json_encode($success);
	}
	else if($lastNameResults)
	{
		echo json_encode($success);
	}
	else if($interestResults)
	{
		echo json_encode($success);
	}
	else
	{
		$status=array("status"=>"failure");
		echo json_encode($status);
	}

?>
