<?php require 'PHP/header.php'; ?>

<body class="page page-blog-post">
    <a href="#content" class="sr-only">Skip to content</a>

    <?php require 'PHP/navbar.php'; ?>

    <?php require 'PHP/profile_content.php'; ?>

    <!-- FOOTER -->
    <?php require 'PHP/footer.php'; ?>
    <?php require 'PHP/footer_scripts.php'; ?>

    <!-- Page Specific Scripts -->
    <script src="js/profile.js"></script>
</body>

</html>
