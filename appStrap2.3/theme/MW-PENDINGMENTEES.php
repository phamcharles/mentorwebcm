<?php require 'PHP/header.php'; ?>

<body class="page page-features">
    <a href="#content" class="sr-only">Skip to content</a>

    <?php require 'PHP/navbar.php'; ?>

    <div id="content">
        <div class="container">
            <!-- Services -->
            <div class="block features">
                <div class="feature col-sm-6 col-md-3">
                    <img src="img/features/hands.jpg" alt="Feature 3" class="img-responsive" />

                </div>
                <h2 class="title-divider">
                    <span>Find
                        <span class="de-em">Mentees</span>
                    </span>
                    <small></small>
                </h2>

                <table class="table table-hover">
                      <!--
                       -->
                    <tr>
                        <th>Name</th>
                        <th>Education</th>
                        <th>Phone</th>
                        <th>Email</th>
                        <th>Actions</th>
                    </tr>
                    <tbody id="t_content">

                    </tbody>
                    <!-- EXAMPLE
                    <tr>
                        <td>John Doe</td>
                        <td>Physics</td>
                        <td>555-5555</td>
                        <td>john.doe@example.com</td>
                        <td><button type="submit" class="btn btn-primary">Add</button> </td>
                    </tr>
                    -->
                </table>
            </div>
        </div>
    </div>

    <!-- FOOTER -->
    <?php require 'PHP/footer.php'; ?>
    <?php require 'PHP/footer_scripts.php'; ?>

    <!-- Page Specific Scripts -->
    <!-- <script src="js/search.js"></script> -->
    <script src="js/find_mentees.js" type="text/javascript" charset="utf-8" async defer></script>
</body>

</html>