<?php require 'PHP/header.php'; ?>

<body class="page page-index">
    <a href="#content" class="sr-only">Skip to content</a>

    <?php require 'PHP/navbar.php'; ?>
    
    <div id="highlighted">
        <!--
      Showshow - Slider Revolution
      see: plugins/slider-revolution/examples&sources for help
      invoke using data-toggle="slider-rev"
      options can be passed to the slider via HTML5 data- ie. data-startwidth="960"
      -->

        <div class="slider-wrapper tp-banner-container" data-page-class="slider-revolution-full-width-behind slider-appstrap-theme">
            <div class="tp-banner" data-toggle="slider-rev" data-delay="9000" data-startwidth="1100" data-startheight="590" data-fullWidth="off">
                <ul>
                    <!-- SLIDE 1 -->
                    <li class="slide" data-transition="fade" data-slotamount="5" data-masterspeed="1800">
                        <img src="img/patterns/white_wall_hash.png" alt="slidebg1" data-bgfit="normal" data-bgposition="left top" data-bgrepeat="repeat">

                        <!-- SLIDE 1 Content-->
                        <div class="slide-content">
                            <!--elements within .slide-content are pushed below navbar on "behind"-->
                            <div class="tp-caption sfb ltl" data-x="10" data-y="50" data-speed="400" data-start="800" data-easing="Back.easeOut" data-endspeed="500" data-endeasing="Back.easeIn" data-captionhidden="on">
                                <img src="img/slides/mentoring3-3.jpg" alt="Mentorship Illustration" />
                                <!--<img src="img/slides/slide1.png" alt="Slide 1"  />-->
                            </div>
                            <h2 class="tp-caption customin randomrotateout" data-x="700" data-y="120" data-speed="400" data-start="1200" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-endspeed="400" data-endeasing="Back.easeIn">MentorWeb online mentorship</h2>
                            <p class="tp-caption customin randomrotateout" data-x="700" data-y="190" data-speed="400" data-start="1600" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-endspeed="400" data-endeasing="Back.easeIn">The best place to get help or give help!</p>
                            <!--data-endeasing="Back.easeIn">Perfect for your App, Web service or hosting company!</p>-->
                            <a href="#" class="tp-caption customin randomrotateout btn btn-lg btn-primary" data-x="700" data-y="220" data-speed="400" data-start="1800" data-customin="x:0;y:0;z:0;rotationX:0;rotationY:0;rotationZ:0;scaleX:0;scaleY:0;skewX:0;skewY:0;opacity:0;transformPerspective:600;transformOrigin:50% 50%;" data-easing="Back.easeOut" data-endspeed="400" data-endeasing="Back.easeIn" id="joinnow_submit">Join Now!</a> 
                        </div>
                        <!------------------------------------LINK TO SIGN UP BUTTON--------------------------------------->
                        <!--data-endeasing="Back.easeIn">Buy Now</a> </div>-->
                    </li>
                    <!-- SLIDE 2 -->
                    <li data-transition="curtain-1" data-slotamount="4" data-masterspeed="500">
                        <img src="img/patterns/lightpaperfibers.png" alt="slidebg1" data-bgfit="normal" data-bgposition="left top" data-bgrepeat="repeat">

                        <!-- SLIDE 2 Content -->
                        <div class="slide-content">
                            <!--elements within .slide-content are pushed below navbar on "behind"-->
                            <div class="tp-caption sfb ltr" data-x="left" data-y="bottom" data-speed="900" data-start="1200" data-easing="Elastic.easeOut" data-endspeed="200" data-endeasing="Power0.easeInOut">
                                <img src="img/slides/mentoring2.jpg" alt="Mentor1" />
                                <!--<img src="img/slides/slide2-layer1.png" alt="Slide 2 layer 1" />-->
                            </div>
                            <div class="tp-caption sfb ltr" data-x="right" data-y="bottom" data-speed="700" data-start="1500" data-easing="Elastic.easeOut" data-endspeed="200" data-endeasing="Power0.easeInOut">
                                <img src="img/slides/mentorshipahead.jpg" alt="Mentor2" />
                                <!--<img src="img/slides/slide2-layer2.png" alt="Slide 2 layer 2" />-->
                            </div>
                            <div class="tp-caption sfb ltr" data-x="center" data-y="bottom" data-speed="900" data-start="1900" data-easing="Elastic.easeOut" data-endspeed="200" data-endeasing="Power0.easeInOut">
                                <img src="img/slides/mentoring3-2.jpg" alt="Mentor3" />
                                <!--<img src="img/slides/slide2-layer3.png" alt="Slide 2 layer 3" />-->
                            </div>
                            <h2 class="tp-caption largeblackbg sfb randomrotateout" data-x="center" data-y="30" data-speed="1500" data-start="2300" data-easing="Elastic.easeOut" data-endspeed="200" data-endeasing="Power0.easeInOut">MentorWeb online mentorship</h2>
                        </div>
                    </li>
                </ul>
                <div class="tp-bannertimer tp-bottom"></div>
            </div>
            <!--end of tp-banner-->
        </div>
    </div>
    <div id="content">
        <div class="container">
            <!-- Services -->
            <div class="block features">
                <h2 class="title-divider">
                    <span>Core
                        <span class="de-em">Features</span>
                    </span>
                    <small>Core features included in all plans.</small>
                </h2>
                <div class="row">
                    <div class="feature col-sm-6 col-md-3">
                        <a href="features.php">
                            <img src="img/features/feature-1.png" alt="Feature 1" class="img-responsive" />
                        </a>
                        <h3 class="title"><a href="features.php">Mobile <span class="de-em">Friendly</span></a>
                        </h3>
                        <p>Need quick access on the go? MentorWeb is 100% mobile friendly and is supported in most phones and tablets</p>
                        <!--<p>Rhoncus adipiscing, magna integer cursus augue eros lacus porttitor magna. Dictumst, odio! Elementum tortor sociis in eu dis dictumst pulvinar lorem nec aliquam a nascetur.</p>-->
                    </div>
                    <div class="feature col-sm-6 col-md-3">
                        <a href="features.php">
                            <img src="img/features/feature-2.png" alt="Feature 2" class="img-responsive" />
                        </a>
                        <h3 class="title"><a href="features.php">24/7 <span class="de-em">Support</span></a>
                        </h3>
                        <p>Contact us at anytime by phone or email and we will respond promptly</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER -->
    <?php require 'PHP/footer.php'; ?>
    <?php require 'PHP/footer_scripts.php'; ?>

    <!-- Page Specific Scripts -->
    <script src="js/joinnow.js"></script>
</body>

</html>