<?php require 'PHP/header.php'; ?>

<body class="page page-signup">
    <a href="#content" class="sr-only">Skip to content</a>

    <?php require 'PHP/navbar.php'; ?>
    
    <div id="content">
        <div class="container">
            <!-- Sign Up form -->
            <form class="form-login form-wrapper form-medium" role="form" id="signup_form">
                <h3 class="title-divider">
                    <span>Sign Up</span>
                    <small>Already signed up? <a href="MW-LOGIN.php">Login here</a>.</small>
                </h3>
                <h5>Account Information</h5>
                <div class="form-group">
                    <label class="sr-only" for="signup_fn">First Name</label>
                    <input type="text" class="form-control" id="signup_fn" name="signup_fn" placeholder="First name">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="signup_ln">Last Name</label>
                    <input type="text" class="form-control" id="signup_ln" name="signup_ln" placeholder="Last name">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="signup_un">Userame</label>
                    <input type="text" class="form-control" id="signup_un" name="signup_un" placeholder="Username">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="signup_eml">Email address</label>
                    <input type="email" class="form-control" id="signup_eml" name="signup_eml" placeholder="Email address">
                </div>
                <div class="form-group">
                    <label class="sr-only" for="signup_pw">Password</label>
                    <input type="password" class="form-control" id="signup_pw" name="signup_pw" placeholder="Password">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox" name='signup_agreement' checked>I agree with the Terms and Conditions.</label>
                </div>
                <button class="btn btn-primary" type="button" id="signup_submit">Sign up</button>
            </form>
        </div>
    </div>

    <!-- FOOTER -->
    <?php require 'PHP/footer.php'; ?>
    <?php require 'PHP/footer_scripts.php'; ?>

    <!-- Page Specific Scripts -->
    <script src="js/signup.js"></script>
</body>

</html>