<?php require 'PHP/header.php'; ?>

<body class="page page-team">
    <a href="#content" class="sr-only">Skip to content</a>

    <?php require 'PHP/navbar.php'; ?>
    
    <div id="content">
        <div class="container" id="about">
            <div class="row">
                <!-- sidebar -->
                <div class="col-md-3 sidebar">
                    <div class="section-menu">
                        <ul class="nav nav-list">
                            <li class="nav-header">In This Section</li>
                            <li><a href="MW-ABOUT.php" class="first">About Us <small>How It All Began</small><i class="fa fa-angle-right"></i></a>
                            </li>
                            <li class="active"><a href="MW-TEAM.php">The Team <small>Our team of stars</small><i class="fa fa-angle-right"></i></a>
                            </li>
                            <li><a href="MW-CONTACT.php">Contact Us<small>How to get in touch</small><i class="fa fa-angle-right"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <!--main content-->
                <div class="col-md-9">
                    <h2 class="title-divider">
                        <span>The
                            <span class="de-em">Team</span>
                        </span>
                        <small>Our team of stars!</small>
                    </h2>

                    <!-- The team -->
                    <div class="block team margin-top-large" id="team">
                        <div class="media">
                            <div class="pull-left">
                                <img src="img/team/daniel.jpg" class="img-thumbnail media-object" alt="Jimi" />
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Daniel</h4>
                                <p class="role">Founder & developer</p>
                                <p>Daniel Torrefranca is a Computer Engineering major at San Jose State University. Works at VSP Global in Information Security and Network Security.</p>
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="media">
                            <div class="pull-left">
                                <img src="img/team/josh.jpg" class="img-thumbnail media-object" alt="Adele" />
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Josh</h4>
                                <p class="role">Founder & developer</p>
                                <p>Josh Ambion is a Computer Engineering major at San Jose State University. He enjoys long walks on the beach, reading poetry, and giving girls flowers.</p>
                                <ul class="list-inline">
                                    <li><a href="https://www.linkedin.com/profile/view?id=155848942&authType=name&authToken=_N_o&trk=prof-connections-name"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="media">
                            <div class="pull-left">
                                <img src="img/team/eduardo.jpg" class="img-thumbnail media-object" alt="Bono" />
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Eduardo</h4>
                                <p class="role">Founder & developer</p>
                                <p>Eduardo Espericueta is a Computer Engineering major at San Jose State University.</p>
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="media">
                            <div class="pull-left">
                                <img src="img/team/charles.jpg" class="img-thumbnail media-object" alt="Robert" />
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">Charles</h4>
                                <p class="role">Founder & developer</p>
                                <p>Charles Pham is a Software Engineering major at San Jose State University. He has worked as Firmware Engineer Intern with Tesla Motors, and also serves as the Executive Advisor for the Society of Computer Engineering.</p>
                                <ul class="list-inline">
                                    <li><a href="#"><i class="fa fa-linkedin"></i> LinkedIn</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER -->
    <?php require 'PHP/footer.php'; ?>
    <?php require 'PHP/footer_scripts.php'; ?>

    <!-- Page Specific Scripts -->
</body>

</html>
