<?php require 'PHP/header.php'; ?>

<body class="page page-features">
    <a href="#content" class="sr-only">Skip to content</a>

    <?php require 'PHP/navbar.php'; ?>

    <div id="content">
        <div class="container">
            <h2 class="title-divider">
                <span>MentorWeb
                    <span class="de-em"></span>
                </span>
                <small><a href="MW-HOME.php"><i class="fa fa-arrow-left"></i> Back to Home Page</a>
                </small>
            </h2>
            <div class="row">
                
                <!--Main Content-->
                <div class="col-md-9 blog-post">

                    <!-- Blog post -->
                    <div class="media row">
                        <div class="col-md-11 media-body">
                            <!--<div class="tags"><a href="#" class="tag">MentorWeb</a> / <a href="#" class="type">profile</a></div>-->
                            <h3 class="title media-heading">Your Messages</h3>

                            <!--Comments-->
                            <div class="comments" id="comments">
                                <!--<h3>Comments (10)</h3>-->
                                <ul class="media-list" id="searchresult_form">
                                    <?php require_once "PHP/messages.php" ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER -->
    <?php require 'PHP/footer.php'; ?>
    <?php require 'PHP/footer_scripts.php'; ?>

    <!-- Page Specific Scripts -->
</body>

</html>