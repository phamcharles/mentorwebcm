<?php require 'PHP/header.php'; ?>

<body class="page page-features">
    <a href="#content" class="sr-only">Skip to content</a>

    <?php require 'PHP/navbar.php'; ?>

    <div id="content">
        <div class="container">
            <!-- Services -->
            <div class="block features">
                <h2 class="title-divider">
                    <span>Mentor
                        <span class="de-em">Center</span>
                    </span>
                    <small>Manage your mentor account</small>
                </h2>
                <form id="search_form" action="#" class="bordered-top-medium" role="form">
                    <div class="input-group">
                        <label class="sr-only" for="callback-number">Search for a Mentee</label>
                        <input type="tel" class="form-control" id="search_value" name="search_value" placeholder="Search for a Mentee">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" id="search_submit">Search</button>
                        </span>
                    </div>
                </form>
                <br>
                <br>
                <div class="row list-unstyled">
                    <div class="feature col-sm-6 col-md-3">
                        <img src="img/features/message3.png" alt="Feature 1" class="img-responsive" />
                        <h3 class="title"><a href="MW-MESSAGECENTERMENTOR.php">Messages<!-- <span class="de-em">Friendly</span>--></a>
                        </h3>
                        <p>Manage your messages here</p>
                    </div>
                    <div class="feature col-sm-6 col-md-3">
                        <img src="img/features/mentees.jpg" alt="Feature 2" class="img-responsive" />
                        <h3 class="title"><a href="MW-MYMENTEES.php">My <span class="de-em">Mentees</span></a>
                        </h3>
                        <p>View your current mentees</p>
                    </div>
                    <div class="feature col-sm-6 col-md-3">
                        <img src="img/features/hands.jpg" alt="Feature 3" class="img-responsive" />
                        <h3 class="title"><a href="MW-PENDINGMENTEES.php">Find a <span class="de-em">Mentees</span></a>
                        </h3>
                        <p>See if anyone wants you as their mentor!</p>
                    </div>
                    <!--
        <div class="feature col-sm-6 col-md-3"> <img src="img/features/feature-4.png" alt="Feature 4" class="img-responsive" />
          <h3 class="title"><a href="#">99.9% <span class="de-em">Uptime</span></a></h3>
          <p>Rhoncus adipiscing, magna integer cursus augue eros lacus porttitor magna. Dictumst, odio! Elementum tortor sociis in eu dis dictumst pulvinar lorem nec aliquam a nascetur.</p>
        </div>
    -->
                </div>
                <div class="row">
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER -->
    <?php require 'PHP/footer.php'; ?>
    <?php require 'PHP/footer_scripts.php'; ?>

    <!-- Page Specific Scripts -->
    <script src="js/search.js"></script>
</body>

</html>