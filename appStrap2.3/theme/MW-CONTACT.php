<?php require 'PHP/header.php'; ?>

<body class="page page-contact">
    <a href="#content" class="sr-only">Skip to content</a>
    
    <?php require 'PHP/navbar.php'; ?>

    <div id="content">
        <div class="container">
            <div class="row">
                <!-- sidebar -->
                <div class="col-md-3 sidebar">
                    <div class="section-menu">
                        <ul class="nav nav-list">
                            <li class="nav-header">In This Section</li>
                            <li><a href="MW-ABOUT.php" class="first">About Us <small>How It All Began</small><i class="fa fa-angle-right"></i></a>
                            </li>
                            <li><a href="MW-TEAM.php">The Team <small>Our team of stars</small><i class="fa fa-angle-right"></i></a>
                            </li>
                            <li class="active"><a href="MW-CONTACT.php">Contact Us<small>How to get in touch</small><i class="fa fa-angle-right"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <!--main content-->
                <div class="col-md-9">
                    <h2 class="title-divider">
                        <span>Contact
                            <span class="de-em">Us</span>
                        </span>
                        <small>Ways To Get In Touch</small>
                    </h2>
                    <div class="row">
                        <div class="col-md-6">
                            <form id="contact-form" action="#" role="form">
                                <div class="form-group">
                                    <label class="sr-only" for="contact-name">Name</label>
                                    <input type="text" class="form-control" id="contact-name" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="contact-email">Email</label>
                                    <input type="email" class="form-control" id="contact-email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label class="sr-only" for="contact-message">Message</label>
                                    <textarea rows="12" class="form-control" id="contact-message" placeholder="Message"></textarea>
                                </div>
                                <input type="button" class="btn btn-primary" value="Send Message">
                            </form>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <abbr title="Phone"><i class="fa fa-phone"></i>
                                </abbr>1-800-MENTORME</p>
                            <p>
                                <abbr title="Email"><i class="fa fa-envelope"></i>
                                </abbr>info@mentorweb.com</p>
                            <p>
                                <abbr title="Address"><i class="fa fa-home"></i>
                                </abbr>San Jose, CA 95192</p>
                            <p>
                                <a href="https://www.google.com/maps/place/Engineering+Bldg/@37.3349481,-121.8879385,14z/data=!4m2!3m1!1s0x808fccbf2a511519:0x480e1d0c4422ad52">
                                    <img src="img/misc/SJMAP" alt="Location map" class="img-thumbnail" />
                                </a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER -->
    <?php require 'PHP/footer.php'; ?>
    <?php require 'PHP/footer_scripts.php'; ?>

    <!-- Page Specific Scripts -->
</body>

</html>
