<?php require 'PHP/header.php'; ?>

<body class="page page-features">
	<a href="#content" class="sr-only">Skip to content</a>

	<?php require 'PHP/navbar.php'; ?>

	<div id="content">
		<div class="container">
			<!-- Services -->
			<div class="block features">
				<h2 class="title-divider">
					<span>My
						<span class="de-em">Mentors</span>
					</span>
				</h2>

				<table class="table table-hover">
					  <!--
					   -->
					<tr>
						<th>Name</th>
						<th>Education</th>
						<th>Phone</th>
						<th>Email</th>
						<th>Actions</th>
					</tr>
					<tbody id="t_content">

					</tbody>
					<!-- EXAMPLE
					<tr>
						<td>John Doe</td>
						<td>Physics</td>
						<td>555-5555</td>
						<td>john.doe@example.com</td>
						<td><button type="submit" class="btn btn-primary">Add</button> </td>
					</tr>
					-->
				</table>
			</div>
		</div>
	</div>

	<!-- FOOTER -->
	<?php require 'PHP/footer.php'; ?>
	<?php require 'PHP/footer_scripts.php'; ?>
	<!-- Page Specific Scripts -->
    <script type="text/javascript" src="js/my_mentors.js"></script>
</body>

</html>