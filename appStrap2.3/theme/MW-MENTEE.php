<?php require 'PHP/header.php'; ?>

<body class="page page-features">
    <a href="#content" class="sr-only">Skip to content</a>

    <?php require 'PHP/navbar.php'; ?>

    <div id="content">
        <div class="container">
            <!-- Services -->
            <div class="block features">
                <h2 class="title-divider">
                    <span>Mentee
                        <span class="de-em">Center</span>
                    </span>
                    <small>Manage your mentee account</small>
                </h2>
                <form id="search_form" action="#" class="bordered-top-medium" role="form">
                    <div class="input-group">
                        <label class="sr-only" for="callback-number">Search for a Mentor</label>
                        <input type="tel" class="form-control" id="search_value" name="search_value" placeholder="Search for a Mentor">
                        <span class="input-group-btn">
                            <button class="btn btn-primary" type="button" id="search_submit">Search</button>
                        </span>
                    </div>
                </form>
                <br>
                <br>
                <div class="row list-unstyled">
                    <div class="feature col-sm-6 col-md-3">
                        <img src="img/features/message3.png" alt="Feature 1" class="img-responsive" />
                        <h3 class="title"><a href="MW-MESSAGECENTERMENTEE.php">Messages<!-- <span class="de-em">Friendly</span>--></a>
                        </h3>
                        <p>Manage your messages here</p>
                    </div>
                    <div class="feature col-sm-6 col-md-3">
                        <img src="img/features/professor.png" alt="Feature 2" class="img-responsive" />
                        <h3 class="title"><a href="MW-MYMENTORS.php">My <span class="de-em">Mentors</span></a>
                        </h3>
                        <p>View your current mentors</p>
                    </div>
                    <div class="feature col-sm-6 col-md-3">
                        <img src="img/features/hands.jpg" alt="Feature 3" class="img-responsive" />
                        <h3 class="title"><a href="MW-PENDINGMENTORS.php">Find a <span class="de-em">Mentors</span></a>
                        </h3>
                        <p>See if anyone wants you as their mentee!</p>
                    </div>
                </div>
                <div class="row">
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER -->
    <?php require 'PHP/footer.php'; ?>
    <?php require 'PHP/footer_scripts.php'; ?>

    <!-- Page Specific Scripts -->
</body>

</html>