<?php require 'PHP/header.php'; ?>

<body class="page page-about">
    <a href="#content" class="sr-only">Skip to content</a>

    <?php require 'PHP/navbar.php'; ?>

    <div id="content">
        <div class="container" id="about">
            <div class="row">
                <!-- sidebar -->
                <div class="col-md-3 sidebar">
                    <div class="section-menu">
                        <ul class="nav nav-list">
                            <li class="nav-header">In This Section</li>
                            <li class="active"><a href="MW-ABOUT.php" class="first">About Us <small>How It All Began</small><i class="fa fa-angle-right"></i></a>
                            </li>
                            <li><a href="MW-TEAM.php">The Team <small>Our team of stars</small><i class="fa fa-angle-right"></i></a>
                            </li>
                            <li><a href="MW-CONTACT.php">Contact Us<small>How to get in touch</small><i class="fa fa-angle-right"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>

                <!--main content-->
                <div class="col-md-9">
                    <h2 class="title-divider">
                        <span>About
                            <span class="de-em">Us</span>
                        </span>
                        <small>What makes us tick!</small>
                    </h2>

                    <!-- About company -->
                    <h4>How It All Began</h4>
                    <p>We, a goup of students, have commited ourselves to helping other peers. Setting time aside to help fellow students and peers with different subjects was a rewarding feeling and so we decided to take in more opportunities to help others when help was called upon. We then envisioned a way of taking this to a global level and say, "Why not help others find and get help through the use of a website?" And so the project took off. Our goal was to create an online hub in which people can be mentored by a professional individual for whatever subject or topic it may be. Thus, MentorWeb was born, giving people all around the world access to help from others who are experienced and willing to also set aside time to spread knowledge to those with the desire to learn. Pressing forward, we would like to continue this service along with future capabilities of live help! Stay tuned, this is just the beginning.</p>
                </div>
            </div>
        </div>
    </div>

    <!-- FOOTER -->
    <?php require 'PHP/footer.php'; ?>
    <?php require 'PHP/footer_scripts.php'; ?>

    <!-- Page Specific Scripts -->
</body>

</html>