var ajax = new AjaxCommunicator();

$("#search_submit").on("click", function()
{
	var msg = $("#search_form").serialize();
	
	ajax.send("PHP/search.php", function(jsonData)
	{
		console.log("After Search: " + jsonData);
		var data = $.parseJSON(jsonData);
		if (data["status"] == "success"){
			location.href = "MW-SEARCH.php";
		}
		else{
			bootbox.alert("<h1 style='color:#478c81'>Search Results</h1>" +
					"<li>No results found.</li>");
		}
	}, msg);
});

