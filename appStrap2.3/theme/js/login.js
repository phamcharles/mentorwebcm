var ajax = new AjaxCommunicator();

//Signup form event listener
$("#login_submit").on("click", function()
{
	var msg = $("#login_form").serialize();
	ajax.send("PHP/login.php", function(sData){
		var data = $.parseJSON(sData);
		console.log("Server return data: " + sData + " " + data);
		if (data['status'] == 'success'){
			bootbox.alert("<h1 style='color:$478c81'>Successful Login</h1>", function(){
				location.href = "MW-HOME.php";
			});				
		}
		else if (data['status'] == 'failure'){
			var lnErrors = "<h1 style='color:#478c81'>LoginProblem:</h1><ul>" +
					"<li>Incorrect username and password combination, please log in again.</li></ul>";
			bootbox.alert(lnErrors, function(){
				location.href = "MW-LOGIN.php";
			});
		}
	}, msg);
});
