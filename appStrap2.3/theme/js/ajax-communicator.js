/** AJAX-COMMUNICATOR **/

function AjaxCommunicator() {
	this.default_error_function = function(e) {
		bootbox.alert('Error: Failed to contact server. Refresh page and try again.');
	};
	this.result = "";
	this.ajax_template = {
		type: 'POST',
		url: undefined,
		data: undefined,
		success: undefined,
		error: this.default_error_function,
		dataType: "text",
		async:false
	};
}
AjaxCommunicator.prototype.send = function(url, success_function, message, error_function, async) {
	var payload = this.ajax_template;
	var callback = this;
	console.log(message);
	if(!objExist(url)) {
		console.error("url missing, aborting send.");
		return;
	}
	if(!objExist(message)) {
		message = "";
	}
	if(objExist(success_function)) {
		payload.success = function(data) {
			success_function(data);
			callback.result = data;
		};
	} else {
		console.error("success function missing, aborting send.");
		return;
	}
	if(objExist(error_function)) {
		payload.error = error_function;
	}
	if(objExist(async)) {
		payload.async = async;
	}
	payload.url = url;
	payload.data = message;

	//console.log(payload);

	$.ajax(payload);
}

AjaxCommunicator.prototype.membershipSignup = function(data, callback, failure) {
	var success = function (data) {
		console.log("[SCE SIGN UP] = "+data);
		if(data.contains("SUCCESS")) {
			if(objExist(callback)) { callback(); }
		} else {
			if(objExist(failure)) { failure(data); }
		}
	};
	this.send("php/sce_registration.php", success, data, undefined, true);
}