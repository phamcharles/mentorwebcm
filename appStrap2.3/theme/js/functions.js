(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);

Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

function getRandomInt(min, max) {
	return Math.floor(Math.random() * (max - min + 1)) + min;
}
/* check if string is empty */
function isEmpty(str) {
	if(!objExist(str)) { return true; }
	try {
		str = str.replace(/[ ]/g, "");
		if(str == "") { return true; }
		return false;
	} catch(e) {
		return false;
	}
}
function objExist(obj) {
	if(typeof obj != "undefined") {
		if(obj != null) {
			return true;
		}
		return false;
	}
	return false;
}
function emptySet(obj) {
	if(Array.isArray(obj)) {
		if(obj.length == 0) {
			return true;
		}
		return false;
	}
	return false;
}
function urlcheck(str){
  return /^http\:\/\/[a-zA-Z0-9\-\.]+\.[a-zA-Z]{2,3}(\/\S*)?$/.test(str);
}
String.prototype.contains = function(search, case_sense) {
	if(case_sense) { search = search.toLowerCase(); this.toLowerCase(); }
	return (this.indexOf(search) != -1);
};
function msg_f(action,data) {
	return "action="+action+"&data="+data;
}
/*function echeck(str)
{
	if(typeof str == 'undefined') { return false; }
	var at="@";
	var dot=".";
	var lat=str.indexOf(at);
	var lstr=str.length;
	var ldot=str.indexOf(dot);

	if (str.indexOf(at)==-1) { return false; }
	if (str.indexOf(at)==-1 || str.indexOf(at)==0 || str.indexOf(at)==lstr) { return false; }
	if (str.indexOf(dot)==-1 || str.indexOf(dot)==0 || str.indexOf(dot)==lstr) { return false; }
	if (str.indexOf(at,(lat+1))!=-1) { return false; }
	if (str.substring(lat-1,lat)==dot || str.substring(lat+1,lat+2)==dot) { return false; }
	if (str.indexOf(dot,(lat+2))==-1) { return false; }
	if (str.indexOf(" ")!=-1) { return false; }

 	return true;
}*/
/*
	Checks if an email is valid.
	@param str email address string.
*/
function echeck(str) {
    var emailText = str;
    var pattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
    if (pattern.test(emailText)) {
        return true;
    } else {
        return false;
    }
}
function includes(arr,obj) {
    return (arr.indexOf(obj) != -1);
}
function removeFromArray(arr, obj) {
    obj = JSON.stringify(obj);
    for (var i=0, max = arr.length; i < max; i++) {
        if (JSON.stringify(arr[i]) === obj)
            arr.splice(i, 1);
    }
}
function arrayFormat(arr) {
	var r_arr = [];
	for(var i = 0; i < arr.length; i++ ) {
		r_arr.push(arr[i].toUpperCase());
	}
	return r_arr;
}
function isBlank(str) {
	if(typeof str == "undefined") {
		return true;
	}
	var n_str = str.trim();
	if(n_str == "") {
		return true;
	} else { return false; }
}

//
// http://thecodeabode.blogspot.com
// @author: Ben Kitzelman
// @license: FreeBSD: (http://opensource.org/licenses/BSD-2-Clause) Do whatever you like with it
// @updated: 03-03-2013
//
var getAcrobatInfo = function() {

	var getBrowserName = function() {
		return this.name = this.name || function() {
		var userAgent = navigator ? navigator.userAgent.toLowerCase() : "other";

		if(userAgent.indexOf("chrome") > -1) return "chrome";
		else if(userAgent.indexOf("safari") > -1) return "safari";
		else if(userAgent.indexOf("msie") > -1) return "ie";
		else if(userAgent.indexOf("firefox") > -1) return "firefox";
		return userAgent;
		}();
	};

	var getActiveXObject = function(name) {
		try { return new ActiveXObject(name); } catch(e) {}
	};

	var getNavigatorPlugin = function(name) {
		for(key in navigator.plugins) {
			var plugin = navigator.plugins[key];
			if(plugin.name == name) return plugin;
		}
	};

	var getPDFPlugin = function() {
		return this.plugin = this.plugin || function() {
			if(getBrowserName() == 'ie') {
			//
			// load the activeX control
			// AcroPDF.PDF is used by version 7 and later
			// PDF.PdfCtrl is used by version 6 and earlier
				return getActiveXObject('AcroPDF.PDF') || getActiveXObject('PDF.PdfCtrl');
			}
			else {
				return getNavigatorPlugin('Adobe Acrobat') || getNavigatorPlugin('Chrome PDF Viewer') || getNavigatorPlugin('WebKit built-in PDF');
			}
		}();
	};

	var isAcrobatInstalled = function() {
		return !!getPDFPlugin();
	};

	var getAcrobatVersion = function() {
		try {
			var plugin = getPDFPlugin();

			if(getBrowserName() == 'ie') {
			var versions = plugin.GetVersions().split(',');
			var latest = versions[0].split('=');
			return parseFloat(latest[1]);
		}

		if(plugin.version) return parseInt(plugin.version);
			return plugin.name
		}
		catch(e) {
			return null;
		}
	}

	//
	// The returned object
	//
	return {
		browser: getBrowserName(),
		acrobatExists: isAcrobatInstalled() ? true : false,
		acrobatVersion: getAcrobatVersion()
	};
};
/*
function cleanAndJSONifyData(data, obj_key) {
	var JSONFlag = new RegExp("JSON\:");
	if(JSONFlag.test(data)) {
		console.log("\tPRE-FINAL: "+data);
		data = data.substr(data.indexOf("JSON:")+"JSON:".length);
		if(data == null || data.contains("FAILURE")) {
			data_class[obj_key] = undefined;
			return false;
		} else if(data.contains("SIGNOUT")) {
			location.hash = "#signout?force";
			return false;
		} else {
			//console.log("\tFINAL: "+data);
			data_class[obj_key] = JSON.parse(data);
			return true;
		}
	} else {
		data_class[obj_key] = undefined;
		return false;
		//location.hash = "";
	}
}*/

function changeNavTo(hash) {
	$( "ul.nav li" ).each(function( index ) {
		$( this ).removeClass();
	});
	$("ul.nav li a[href^='"+hash+"']").parent().addClass("active");
}


function getUrlParameter(param) {
	var loc = location.href.indexOf("?");
	if(loc == -1) { return ""; }
	var hash = location.href.substr(loc);
	var start = hash.indexOf(param)+param.length+1;
	var end = hash.indexOf("&", start);
	var value = "";
	if(end != -1) {
		value = hash.substr(start, end-start);
	} else {
		value = hash.substr(start);
	}
	return value;
}
function changeUrlParameter(param, value) {
	var loc = location.href.indexOf("?");
	if(loc == -1) { return ""; }
	var hash = location.href.substr(loc);
	var end = hash.indexOf("&", start);
	var new_hash = "";
	if(end != -1) {
		new_hash = hash.substr(0, start)+value+hash.substr(end);
	} else {
		new_hash = hash.substr(0,start)+value;
	}
	location.hash = new_hash;
	return true;
}
function composeErrorMsg(data) {
	data = data.substr(data.indexOf("INVALID:")+"INVALID:".length);
	var error = "<h1>Signup Problems:</h1><ul>";
	if(data.contains("fname")) {
		error += "<li>Invalid first name. Should only have letters.</li>";
	}
	if(data.contains("mname")) {
		error += "<li>Invalid middle initial.</li>";
	}
	if(data.contains("lname")) {
		error += "<li>Invalid last name. Should only have letters.</li>";
	}
	if(data.contains("id")) {
		error += "<li>Invalid ID. Should only contain 9 numbers.</li>";
	}
	if(data.contains("ieee")) {
		error += "<li>Invalid IEEE Number. Should only contain n numbers.</li>";
	}
	if(data.contains("memail")) {
		error += "<li>Invalid email.</li>";
	}
	error += "</ul>";
	return error;
}

function returnValidField(data) {
	if(objExist(data)) {
		return data;
	} else { return "n/a"; }
}