var ajax = new AjaxCommunicator();
var url_id = getUrlParameter("_id");

$("#add_mentee").on("click", function(){
	var msg = "action=addMentee&" + "data=" + url_id + "&";
	ajax.send("PHP/add_remove_users.php", function(sData){
		if (sData.contains("SUCCESS")){
			bootbox.alert("They have become your mentee");
		}
	}, msg);
});

$("#add_mentor").on("click", function(){
	var msg = "action=addMentor&" + "data=" + url_id;
	ajax.send("PHP/add_remove_users.php", function(sData){
		if (sData.contains("SUCCESS")){
			bootbox.alert("They have become your mentor");
		}
	}, msg);
});

$("#remove_mentee").on("click", function(){
	var msg = "action=removeMentee&" + "data=" + url_id + "&";
	ajax.send("PHP/add_remove_users.php", function(sData){
		if (sData.contains("SUCCESS")){
			bootbox.alert("You have removed your mentee");
		}
	}, msg);
});

$("#remove_mentor").on("click", function(){
	var msg = "action=removeMentor&" + "data=" + url_id;
	ajax.send("PHP/add_remove_users.php", function(sData){
		if (sData.contains("SUCCESS")){
			bootbox.alert("You have removed your mentor");
		}
	}, msg);
});