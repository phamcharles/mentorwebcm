

var ajax = new AjaxCommunicator();

$(document).ready(function() {
	var success = function(data) {
		console.log(data);
		var struct = JSON.parse(data);

		console.debug(struct);

		var table_html = [
			"<tr>"+
			"	<td>", /* name */ "</td>"+
			"	<td>", /* education */ "</td>"+
			"	<td>", /* phone */ "</td>"+
			"	<td>", /* email */ "</td>"+
			"	<td><a class='btn btn-primary' href='MW-PROFILE.php?type=mentor&id=", /* id */ "'>View Profile</a></td>"+
			"</tr>"
		];
		var insert = "";
		for (var i = 0; i < struct.length; i++) {
			insert += table_html[0]+returnValidField(struct[i]["name"]["first"])+" "+returnValidField(struct[i]["name"]["last"]);
			insert += table_html[1]+returnValidField(struct[i]["education"]);
			insert += table_html[2]+returnValidField(struct[i]["contact"]["phone"]);
			insert += table_html[3]+returnValidField(struct[i]["contact"]["email"]);
			insert += table_html[4]+returnValidField(struct[i]["_id"]["$id"]);
			insert += table_html[5];
		}
		$("#t_content").html(insert);
	};
	var msg = "action=mentors";
	ajax.send("PHP/data_retrieval.php", success, msg);
});