var ajax = new AjaxCommunicator();

//Signup form event listener
$("#profile_edit").on("click",function(event){
	event.preventDefault();
	if ($("#profile_edit").text().contains("Edit Profile")){
		$("#profile_edit").text("Submit");
		var firstname = $("#profile_firstname").html();
		$("#profile_firstname").html("<input class='form-control' style='display:inline-block; width:auto;' value='"+firstname+"' name='text_firstname'>");
		var lastname = $("#profile_lastname").html();
		$("#profile_lastname").html("<input class='form-control' style='display:inline-block; width:auto;' value='"+lastname+"' name='text_lastname'>");		
		var phone = $("#profile_phone").html();
		$("#profile_phone").html("<input class='form-control' style='display:inline-block; width:auto;' value='"+phone+"' name='text_phone'>");	
		var city = $("#profile_city").html();
		$("#profile_city").html("<input class='form-control' style='display:inline-block; width:auto;' value='"+city+"' name='text_city'>");	
		var job = $("#profile_job").html();
		$("#profile_job").html("<input class='form-control' style='display:inline-block; width:auto;' value='"+job+"' name='text_job'>");	
		var education = $("#profile_education").html();
		$("#profile_education").html("<input class='form-control' style='display:inline-block; width:auto;' value='"+education+"' name='text_education'>");	
		var interests = $("#profile_interests").html();
		$("#profile_interests").html("<input class='form-control' style='display:inline-block; width:auto;' value='"+interests+"' name='text_interests'>");	
	}
	else if ($("#profile_edit").text().contains("Submit")){
		var msg = $("#profile_form").serialize();
		console.log(msg);
		ajax.send("PHP/editprofile.php",function(){
			location.href = "MW-EDITPROFILE.php";
		}, msg);		
	}
});
