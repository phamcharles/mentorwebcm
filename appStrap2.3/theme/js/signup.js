var ajax = new AjaxCommunicator();

//Signup form event listener
$("#signup_submit").on("click", function()
{
	var fn = $("#signup_form input[name='signup_fn']").val();
	var ln = $("#signup_form input[name='signup_ln']").val();
	var un = $("#signup_form input[name='signup_un']").val();
	var eml = $("#signup_form input[name='signup_eml']").val();
	var pw = $("#signup_form input[name='signup_pw']").val();
	var flag = 0;
	var errorHeader = "<h1 style='color:#478c81'>Registration Problems:</h1><ul>";
	var errors = errorHeader;

	//Function requirement parameters
	var namePattern = /^[A-Za-z]{1,30}$/;
	var userPattern = /^[A-Za-z]+[A-Za-z0-9]{1,30}$/;
	var emlPattern = /^[a-zA-Z0-9\-_]+(\.[a-zA-Z0-9\-_]+)*@[a-z0-9]+(\-[a-z0-9]+)*(\.[a-z0-9]+(\-[a-z0-9]+)*)*\.[a-z]{2,4}$/;
	var pwPattern = /^.*(?=.{8,32})(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[\!\@\#\$\%\^\&\*\_\-\+\?]).*$/;

	//Client side validation

	//Terms and condition check
	if (!$("#signup_form input[name='signup_agreement']").is(":checked")){
		errors += "<li>You must agree with our <strong>Terms and Conditions</strong> in order to use our services.</li>";
		flag++;
	}
	//First Name check
	if (!namePattern.test(fn)){
		errors += "<li id='invalid_fn'>Invalid <strong>First Name*</strong>." +
					"<ul style='display:none;'><li>Allows uppercase letters and lowercase letters.</li>" +
						"<li>Disallows digits and symbols.</li></ul></li>";
		flag++;
	}
	//Last Name check
	if (!namePattern.test(ln)){
		errors += "<li id='invalid_ln'>Invalid <strong>Last Name*</strong>." +
					"<ul style='display:none;'><li>Allows uppercase letters and lowercase letters.</li>" +
						"<li>Disallows digits and symbols.</li></ul></li>";
		flag++;
	}
	//Username check
	if (un == 'Username' || !userPattern.test(un)){
		errors += "<li id='invalid_un'>Invalid <strong>Username*</strong>." +
					"<ul style='display:none;'><li>Allows uppercase letters, lowercase letters, and digits.</li>" +
						"<li>Disallow starting with digits.</li></ul></li>";
		flag++;
	}
	//Email check
	if (!emlPattern.test(eml)){
		errors += "<li id='invalid_eml'>Invalid <strong>Email*</strong>." +
					"<ul style='display:none;'><li>Example: user@domain.com.</li></ul></li>";
		flag++;
	}
	//Password check
	if (pw == 'Password' || !pwPattern.test(pw) ){
		errors += "<li id='invalid_pw'>Invalid <strong>Password*</strong>." +
					"<ul style='display:none;'><li>Allows uppercase letters, lowercase letters, digits, and symbols.</li>" +
						"<li>Password requires atleast one symbol.</li>" +
						"<li>Password requires atleast one uppercase letter.</li>" +
						"<li>Password requires atleast one digit.</li></ul></li>";
		flag++;
	}
	
	errors += "<small class='pull-right'>*Click for more details</small></ul>"

	//Validity of signup

	//Error signup
	if (flag > 0){
		//Alert and after closed alert, turn off event listeners
		bootbox.alert(errors, function(){
			$("#invalid_fn").off('click');
			$("#invalid_ln").off('click');
			$("#invalid_un").off('click');
			$("#invalid_eml").off('click');
			$("#invalid_pw").off('click');
		});
		//UX mouseover link
		$("#invalid_fn").css('cursor', 'pointer');
		$("#invalid_ln").css('cursor', 'pointer');
		$("#invalid_un").css('cursor', 'pointer');
		$("#invalid_eml").css('cursor', 'pointer');
		$("#invalid_pw").css('cursor', 'pointer');

		//On click for more details
		$("#invalid_fn").on('click',function(){
			$("#invalid_fn ul").css('cursor', 'default');
			$("#invalid_fn ul").slideDown("fast");
		});
		$("#invalid_ln").on('click',function(){
			$("#invalid_ln ul").css('cursor', 'default');
			$("#invalid_ln ul").slideDown("fast");
		});
		$("#invalid_un").on('click',function(){
			$("#invalid_un ul").css('cursor', 'default');
			$("#invalid_un ul").slideDown("fast");
		});
		$("#invalid_eml").on('click',function(){
			$("#invalid_eml ul").css('cursor', 'default');
			$("#invalid_eml ul").slideDown("fast");
		});
		$("#invalid_pw").on('click',function(){
			$("#invalid_pw ul").css('cursor', 'default');
			$("#invalid_pw ul").slideDown("fast");
		});
	}
	//Success signup
	else {
		var msg = $("#signup_form").serialize();
		
		ajax.send("PHP/signup.php", function(sData)
		{
			var data = $.parseJSON(sData);
			if(data['status'] == 'success'){			
				bootbox.alert("<h1 style='color:#478c81'>Sucessful Registration</h1>", function(){
					location.href = "MW-HOME.php";	
				});
			}
			else{
				errorHeader += "<li>" + data['status'] + "</li></ul>";
				flag++;
				bootbox.alert(errorHeader);
			}
		}, msg);

	}
	});
