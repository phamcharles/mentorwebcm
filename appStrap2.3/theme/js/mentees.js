

var ajax = new AjaxCommunicator();

$(document).ready(function() {
	var success = function(data) {
		console.log(data);
		var struct = JSON.parse(data);

		console.debug(struct);

		var table_html = [
			"<tr>"+
			"	<td>", /* name */ "</td>"+
			"	<td>", /* education */ "</td>"+
			"	<td>", /* phone */ "</td>"+
			"	<td>", /* email */ "</td>"+
			"	<td><a class='btn btn-primary' href='MW-PROFILE.php?type=mentee&id=", /* id */ "'>View Profile</a></td>"+
			"</tr>"
		];
		var insert = "";
		for (var i = 0; i < struct.length; i++) {
			insert += table_html[0]+returnValidField(struct[i]["name"]["first"])+" "+returnValidField(struct[i]["name"]["last"]);
			insert += table_html[1]+returnValidField(struct[i]["education"]);
			insert += table_html[2]+returnValidField(struct[i]["contact"]["phone"]);
			insert += table_html[3]+returnValidField(struct[i]["contact"]["email"]);
			insert += table_html[4]+returnValidField(struct[i]["_id"]["$id"]);
			insert += table_html[5];
		};

		$("#mentees").html(insert);


/*		var insert = "";
		insert += */
	}
	var msg = "action=mentees";
	ajax.send("PHP/data_retrieval.php", success, msg);
});

//Signup form event listener
$("#login_submit").on("click", function()
{
	var msg = $("#login_form").serialize();
	ajax.send("PHP/login.php", function(sData){
		var data = $.parseJSON(sData);
		console.log("Server return data: " + sData + " " + data);
		if (data['status'] == 'success'){
			bootbox.alert("<h1 style='color:$478c81'>Successful Login</h1>", function(){
				location.href = "MW-HOME.php";
			});
		}
		else if (data['status'] == 'failure'){
			var lnErrors = "<h1 style='color:#478c81'>LoginProblem:</h1><ul>" +
					"<li>Incorrect username and password combination, please log in again.</li></ul>";
			bootbox.alert(lnErrors, function(){
				location.href = "MW-LOGIN.php";
			});
		}
	}, msg);
});
